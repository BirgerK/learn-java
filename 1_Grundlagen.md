# Zweck des Projekts
Im Rahmen dieses Projekts, soll ein ein Chat-System entwickelt werden. Das Projekt ist so aufgezogen, dass zunächst einfache Teile entwickelt werden. Im Laufe des Projektes werden Frameworks und Technologien hinzugefügt.

Das Projekt eignet sich zum Lernen jeder Programmiersprache, es wird allerdings konkret auf die Entwicklung mit Java eingehen.

# Grundlagen
In diesem Teil wird das Java-Projekt aufgesetzt und es werden erste Schritte zum Chat-System entwickelt.

## Was du lernst
* Die Java-Syntax
* "Googeln" mit richtigen Suchbegriffen
* Eine CSV-Datei manuell zu parsen

## Aufsetzen des Java-Projektes
Zunächst einige Begriffsdefinitionen:
* `Root`-Ordner oder -Directory -> Das oberste Datei-Verzeichnis eines Projektes
* `Source`-Ordner oder -Directory -> Der Ordner `src`, der sich im Root-Ordner befinden sollte
* `~` -> UNIX-Konvention für den "Ordner des aktuellen Benutzers auf dem Computer". Wenn ich auf einem Windows-PC mit dem Nutzer "Bob" angemeldet bin, steht `~` für den Pfad: `C:/Benutzer/Bob/`

Häufig hat ein Entwickler auf seinem Computer einen Ordner, in dem er standardmäßig alle Projekt anlegt. Als Eclipse-Nutzer ist dieser Pfad häufig `~/workspace`. Ich selbst verwende dafür `~/git`. Es ist nützlich alle Projekte an einem Punkt zu sammeln oder zu ordnen, dann muss man nicht zu viel danach suchen.

Ich empfehle daher dieses Projekt unter dem Pfad `~/workspace/learn-java` anzulegen. Innerhalb dieses Pfads lege bitte außerdem folgende Ordner-Struktur an:

```
learn-java
  | - src
      | - main
          | - java
```

## Paket-Struktur von Java
Alle Java-Source-Dateien werden in sogenannten `Packages` geordnet. Eine Java-Klasse gehört immer zu einem Package. Sieht man sich eine Java-Klasse an, sieht man ganz oben die Zeile, die das Package beschreibt, in dem es sieht befindet. Beispielsweise: `package de.learn.java;`.

Man muss allerdings nicht nur definieren, dass sich diese Klasse in dem Package befindet, sondern die Source-Datei auch in den entsprechenenden Pfad ablegen. Wir erweitern daher die Datei-Struktur, sodass sie wie folgt aussieht:

```
learn-java
  | - src
      | - main
          | - java
              | - de
                  | - learn
                      | - java
```

## Die ersten Zeilen Code
Ausgehend davon, dass die Ordner-Struktur bereits angelegt ist, können wir anfangen zu coden!

### Importieren des Projekts in Eclipse
Damit wir in Eclipse coden können, müssen wir zunächst das angelegte Projekt in Eclipse anlegen. Dazu google bitte den Suchbegriff `eclipse import java project`. Mit Sicherheit gibt es eine gute Antwort dazu auf stackoverflow.com. Falls nicht, schreib mich ruhig an.

#### Was soll das System können?
Da wir nun in Eclipse coden können, können wir endlich anfangen.

Zunächst soll das System die mitgelieferte "Comma-Separated-Value"-Datei (CSV) einlesen und (zunächst) einfach ausgeben.

Es gibt Java-eigene Mittel um Dateien einzulesen und Zeile-pro-Zeile abzuarbeiten. Dazu bitte den Begriff `java read file line per line` googeln.

Eine CSV-Datei enthält pro Zeile einen Datensatz. Die einzelnen Felder ("Attribute") jedes Datensatzes sind mit einem Trennzeichen voneinander getrennt. In diesem Beispiel ist das Trennzeichen ein Semikolon (`;`).

Die folgende Zeile:
```
Bob;Joe;Toller Tag heute;14:55:00
```

Muss daher so getrennt werden, dass sich die Feldwerte `Bob`, `Joe`, `Toller Tag heute` und `14:55:00` ergeben.

Jede dieser Zeilen soll in einem Java-Objekt gespeichert werden. Dazu erstelle die Klasse `Message`. Zu diesem Zeitpunkt könnte deine Ordner-Struktur wie folgt aussehen:

```
learn-java
  | - src
      | - main
          | - java
              | - de
                  | - learn
                      | - java
                          | - Main.java
                          | - MessageReader.java
                          | - Message.java
```

Die Java-Klasse `Message` sollte, entsprechend dem Input, die privaten (!) Attribute `sender`, `recipient`, `message` und `sendTime` mit entsprechenden Getter- und Setter-Methoden besitzen.

Bitte speichere alle `Message`-Objekte zunächst in einer Liste zwischen, damit sie für den nächsten Verarbeitungsschritt alle vorhanden sind.

Anschließend gibst du alle Message-Objekte im JSON-Format auf der Konsole aus, das geht über `System.out.println()`.
